variable "project_id" {
  type    = string
  description = ""
}

variable "project_name" {
  type    = string
  description = ""
}

variable "bucket_name" {
  type    = string
  description = ""
}

variable "function_name" {
  type    = string
  description = ""
}

variable "scheduler_stop_name" {
  type    = string
  description = ""
}

variable "GCP_ENVIRONMENT" {
  type    = string
  description = "Provide cloud sql instance name"
}
