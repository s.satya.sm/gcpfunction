resource "google_pubsub_topic" "topicname" {
  projrct = var.project_id
  name    = "var.pub_sub_name"
}

resource "google_service_account" "name" {
  project    = var.project_id
  account_id = "id"
  display_name = "service account name"
}

 resource "google_project_iam_member" "name" {
  project      = var.project_id
  role         = "roles/cloudsql.admin"
  member       = "serviceAccount: ${google_service_account.name.email}"
}

resource "google_storage_bucket" "bucket" {
  name = "var.bucket_name"
}

data "archive_file" "function_go" {
  type        = "zip"
  source_file = "./files/function.go"
  output_path = "./files/function.zip"
}
#  source {
#    content  = "file("./files/function.go")"
#    filename = "function.go"
#  }
#}

resource "google_storage_bucket_object" "archive" {
  name   = "function.zip"
  bucket = "var.bucket_name"
  source = "./function/function.zip"
  depends_on = [data.archive_file.function_go]
}

resource "google_cloudfunctions_function" "function" {
  project = var.project_id
  name                  = "var.function_name"
  available_memory_mb   = 128
  source_archive_bucket = "var.bucket_name"
  source_archive_object = "${google_storage_bucket_object.archive.name}"
  runtime = "go116"
  timeout     = 60
  entry_point = "ProcessPubSub"

  event_trigger = {
    event_type = "providers/cloud.pubsub/eventTypes/topic.publish"
    resource   = "${google_pubsub_topic.topicname.name}"
    failure_policy = {
      retry = false
    }
  }
}

resource "google_cloudfunction_function_iam_member" "name_invoker" {
project = var.project_id
region  = google_cloudfunctions_function.name.region
cloud_function = google_cloudfunctions_function.name

role = "roles/cloudfunctions.invoker"
member = "serviceAccount: ${google_service_account.name.email}"
}

resource "google_cloud_scheduler_job" "job" {
  name        = "var.scheduler_stop_name"
  description = "Stop the cloudsql instance"
  schedule    = "00 21 * * *"
  time_zone    = "America/Los_Angeles"

  pubsub_target {
    topic_name = google_pubsub_topic.topic.id
    data       = base64encode("{
    \"Instance\": \"var.GCP_ENVIRONMENT\",
    \"Project\": \"var.project_id\",
    \"Action\": \"stop\"}")
  }
}
